package com.example.kasaiyuuka.wetherforcasts;

/**
 * Created by kasaiyuuka on 2015/03/06.
 */

import android.content.Context;
import android.net.http.AndroidHttpClient;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class WeatherApi {

    private static final String USER_AGENT = "WeatherForecasts Sample";

    private static final String URL = "http://weather.livedoor.com/forecast/webservice/json/v1?city=";

    //天気予報を下記URLからよびだす関数
    //public static String getWhether (Context context,String pointId)throws IOException{
    public static WeatherForecast getWeather(Context context,String pointId)throws IOException,JSONException{
        AndroidHttpClient client = AndroidHttpClient.newInstance(USER_AGENT,context);
        //設定したURLにpointId(フィールド（地点）)を足して問い合わせる。
        HttpGet get =new HttpGet(URL + pointId);

        StringBuilder sb = new StringBuilder();
        try {
            HttpResponse response = client.execute(get);
            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String line = null;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        }finally{
            client.close();
        }

        //return sb.toString();
        return new WeatherForecast(new JSONObject(sb.toString()));
    }
}
