package com.example.kasaiyuuka.wetherforcasts;

/**
 * Created by kasaiyuuka on 2015/03/11.
 */

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;

import java.io.IOException;

public class FragmentWeather extends Fragment{
    private static final String KEY_CITY_CODE = "key_city_code";

    public static FragmentWeather newInstance(String cityCode) {
        FragmentWeather fragment = new FragmentWeather();
        Bundle args = new Bundle();
        args.putString(KEY_CITY_CODE, cityCode);
        fragment.setArguments(args);

        return fragment;
    }

    private TextView location;
    private LinearLayout forecastLayout;
    private ProgressBar progress;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my, null);

        //findViewById(ソースコード外のリソースを利用する時に使う
        location = (TextView) view.findViewById(R.id.tv_location);
        forecastLayout = (LinearLayout) view.findViewById(R.id.ll_forecasts);
        progress = (ProgressBar) view.findViewById(R.id.progress);

        new GetWeatherForecastTask(getActivity()).execute(getArguments().getString(KEY_CITY_CODE));

        return view;
    }

    private class GetWeatherForecastTask extends GetWeatherForecastApiTask {
        public GetWeatherForecastTask(Context context) {
            super(context);
        }

        @Override
        //onPreExecute(バックグラウンド処理に入る前に行う処理)
        // progressBarの表示
        protected void onPreExecute() {
            //おまじない（ないとだめ
            super.onPreExecute();
            //ぐるぐるするやつを表示
            progress.setVisibility(View.VISIBLE);
        }

        @Override
        //OnPostExecute(バックグラウンド処理が終わった後に呼ばれるメソッド
        protected void onPostExecute(WeatherForecast data) {

            super.onPostExecute(data);
            //ぐるぐるするやつを消す
            progress.setVisibility(View.GONE);

            if (data != null) {
                location.setText(data.location.area + " " + data.location.prefecture + " " + data.location.city);

                // 予報を一覧表示
                for (WeatherForecast.Forecast forecast : data.forecastList) {
                    View row = View.inflate(getActivity(), R.layout.forecasts_row, null);


                    TextView telop = (TextView) row.findViewById(R.id.tv_telop);
                    telop.setText(forecast.telop);

                    TextView temp = (TextView) row.findViewById(R.id.tv_tempreture);
                    temp.setText(forecast.temperature.toString());

                    ImageView imageView = (ImageView) row.findViewById(R.id.iv_weather);
                    imageView.setTag(forecast.image.url); // 読み込むURLを設定

                    // 読み込み処理の実行
                    ImageLoaderTask task = new ImageLoaderTask(getActivity());
                    task.execute(imageView);

                    forecastLayout.addView(row);
                }

            } else if (exception != null) {
                Toast.makeText(getActivity(), exception.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }
}



