package com.example.kasaiyuuka.slotgame;

import android.app.Application;

/**
 * Created by kasaiyuuka on 2015/03/12.
 */
public class Globals extends Application {
    static int[] stpflg = new int[3];  //リールをとめるフラグ
    static int[] btnflg = new int[3];  //ストップボタン処理
    static int[] pic = new int[3];  //絵
    static int coin;  //コイン枚数
    static int keisanflg;  //計算を1度だけにするフラグ
    static int startflg;  //二度押しさせないフラグ
    static int Atariflg;
    static int fiever;
    static int bet;    //betした回数を記憶
    static int betflg;    //二度押しさせないフラグ
    static int refund; //払い戻し

    public static void GlobalsIsAllInit(){
        for(int i=0;i<stpflg.length;i++){
            btnflg[i]=1;
            stpflg[i]=1;
        }
        betflg=0;
        coin = 20;
        keisanflg=1;
        startflg=1;
        bet=0;
        refund=0;
        fiever=0;
    }
}


